import sublime, sublime_plugin, re

class DuplicateSelectorCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        for region in self.view.sel():
            if region.empty():
                self.view.show('test')